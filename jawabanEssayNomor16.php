<?php

// membuat tabel customers

create database myshop; //membuat database
use myshop;

create table customers (
    id int auto_increment PRIMARY KEY,
    name varchar(255),
    email varchar(255),
    password varchar(255)
);

// membuat table orders
create table orders (
    id int auto_increment PRIMARY KEY,
    amount varchar(255),
    customer_id int,
    CONSTRAINT customer_id FOREIGN KEY (customer_id) REFERENCES customers(id)
);