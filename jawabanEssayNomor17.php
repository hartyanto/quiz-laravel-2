<?php

// menambahkan data pada table customers

INSERT INTO customers VALUES ( '', 'John Doe', 'john@doe.com', 'john123');
INSERT INTO customers VALUES ( '', 'Jane Doe', 'jane@doe.com', 'jenita123');

// menambahkan data pada tabel orders

INSERT INTO orders VALUES ( '', 500, 1);
INSERT INTO orders VALUES ( '', 200, 2);
INSERT INTO orders VALUES ( '', 750, 2);
INSERT INTO orders VALUES ( '', 250, 1);
INSERT INTO orders VALUES ( '', 400, 2);

/*
Alternatif lain
INSERT INTO orders (amount, customer_id) VALUES
    (500, 1),
    (200, 2),
    (750, 2),
    (250, 1),
    (400, 2);

*/